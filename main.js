/**
 * Created by Leonard Cheong on 30/3/2016.
 */
/** loading express after you have done npm install --save express */
var express = require("express");
/** creating an application from the express function */
var app = express();
/** telling the computer to use this directory as the document route */
app.use(express.static(__dirname + "/public"));
console.log("__dirname = " + __dirname + "/public");

/** Starting the webserver on port 3000 */
app.listen(3000, function() {
    console.info("My Web Server has started on port 3000");
    console.info("Document root is at " + __dirname + "/public");
    console.info("Copyright")
}
);

